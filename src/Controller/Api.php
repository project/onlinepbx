<?php

namespace Drupal\onlinepbx\Controller;

/**
 * @file
 * Contains \Drupal\synhelper\Controller\Page.
 */
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for page example routes.
 */
class Api {

  /**
   * Test.
   */
  public static function callNow($to, $from = FALSE) {
    $config = \Drupal::config('onlinepbx.settings');
    if (!$from) {
      $from = $config->get('from');
    }
    \Drupal::moduleHandler()->alter('onlinepbx_from', $from);
    $otvet = "Call Now\n$from => $to\n";
    $data = [
      "from" => $from,
      "to" => $to,
      "from_orig_name" => 'web-site',
    ];
    \Drupal::logger('onlinepbx')->notice("call: $from => $to");
    // Get call history.
    $result = self::request("call/now.json", $data, TRUE);
    $otvet .= print_r($result, TRUE);
    return $otvet;
  }

  /**
   * Test.
   */
  public static function test() {
    $otvet = "callHistory\n-1 day => now\n";
    $data = [
      "date_from" => (new \DateTime())->modify("-1 day")->format("r"),
      "date_to" => (new \DateTime())->format("r"),
    ];
    // Get call history.
    $result = self::requestTest("mongo_history/search.json", $data);
    $otvet .= print_r($result, TRUE);
    return $otvet;
  }

  /**
   * Test.
   */
  public static function requestTest($method, $data, $noResponse = FALSE) {
    $config = \Drupal::config('onlinepbx.settings');
    $url = $config->get('url');
    $key = $config->get('key');
    // Create new client object.
    $client = new ApiClient($url, $key, FALSE, $noResponse);
    $result = $client->sendRequest($method, $data);
    dsm($result);
    dsm(self::toOldData($result));
    return $result;
  }

  /**
   * Test.
   */
  public static function request($method, $data, $noResponse = FALSE) {
    $config = \Drupal::config('onlinepbx.settings');
    $url = $config->get('url');
    $key = $config->get('key');
    // Create new client object.
    $client = new ApiClient($url, $key, FALSE, $noResponse);
    if (!empty($data['date_from'])) {
      $data['start_stamp_from'] = strtotime($data['date_from']);
      $data['start_stamp_to'] = strtotime($data['date_to']);
    }
    $result = $client->sendRequest($method, $data);
    return $result;
  }

  /**
   * New API data to OLD data.
   */
  public static function toOldData($data) {
    $result['status'] = $data['status'];
    // dsm($data);
    foreach ($data['data'] as $key => $value) {
      $result['data'][$key]['uuid'] = $value['uuid'];
      $result['data'][$key]['type'] = $value['accountcode'];
      $result['data'][$key]['caller'] = $value['caller_id_number'];
      $result['data'][$key]['to'] = $value['destination_number'];
      $result['data'][$key]['date'] = $value['start_stamp'];
      $result['data'][$key]['gateway'] = $value['gateway'];
      $result['data'][$key]['duration'] = $value['duration'];
      $result['data'][$key]['billsec'] = $value['user_talk_time'];
      $result['data'][$key]['hangup_cause'] = $value['hangup_cause'];
    }
    return $result;
  }

  /**
   * API is OK.
   */
  public static function isOk($data) {
    $result = FALSE;
    if (isset($data['status']) && $data['status'] == 1) {
      if (is_array($data['data'])) {
        $xdata = self::toOldData($data);
        $result = $xdata['data'];
      }
      else {
        $result = $data['data'];
      }
    }
    return $result;
  }

}
