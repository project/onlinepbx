<?php

namespace Drupal\onlinepbx\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\onlinepbx_phones_migration\Utility\CallRecordFileSave;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for page example routes.
 */
class CallRecord extends ControllerBase {

  /**
   * Test s3.
   */
  public function test($uuid) {
    $url = self::getRecord($uuid);
    $storage = \Drupal::entityTypeManager()->getStorage('phones_call');
    $calls = $storage->loadByProperties(['uuid' => $uuid]);
    if (!empty($calls)) {
      $call = array_shift($calls);
      $result = CallRecordFileSave::mp3Save($call);
    }
    return [
      '#markup' => __CLASS__,
    ];
  }

  /**
   * ONline.
   */
  public function record($uuid) {
    if ($mp3 = $this->getCached($uuid)) {
      \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($uuid ?? [])]
      );
      // Ajax-JSON data.
      if (\Drupal::request()->request->get('_ajax')) {
        \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
          '@j', ['@j' => json_encode('ajax' ?? [])]
        );
        $src = "<source src='/onlinepbx/record/$uuid/rec.mp3' type='audio/mpeg'>";
        // (autoplay|preload='(auto|none|metadata)')
        $play = "preload='metadata'";
        $audio = "<audio id='rec-$uuid' $play controls >$src</audio>";
        $responce = [
          'success' => TRUE,
          'audio' => $audio,
        ];
        return new JsonResponse($responce);
      }
      // Mp3 file Response.
      else {
        \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
          '@j', ['@j' => json_encode("file - $mp3" ?? [])]
        );
        $file = @file_get_contents($mp3, TRUE);
        $filename = "rec-$uuid.mp3";
        $response = new Response($file);
        $response->headers->set('Content-Type', "audio/mpeg");
        $response->headers->set('Content-Disposition', "inline; filename=$filename");
        $response->headers->set('Content-length', strlen($file));
        $response->headers->set('X-Accel-Buffering', "no");
        $response->headers->set('Accept-Ranges', 'bytes');
        return $response;
      }
    }
    return new JsonResponse(['error' => '&nbsp; &nbsp;not found']);
  }

  /**
   * Get cached Record.
   */
  public static function getRecord($uuid) {
    $result = FALSE;
    $data = [
      "uuid" => $uuid,
      "download" => 1,
    ];
    $request = Api::request("mongo_history/search.json", $data);
    if ($mp3 = Api::isOk($request)) {
      $result = $mp3;
    }
    return $result;
  }

  /**
   * Get cached Record.
   */
  private function getCached($uuid) {
    $data = &drupal_static("CallRecord::getCached($uuid)");
    if (!isset($data)) {
      $cache_key = "onlinepbx:rec:$uuid";
      if ($cache = \Drupal::cache()->get($cache_key)) {
        $data = $cache->data;
      }
      elseif ($data = $this->getRecord($uuid)) {
        \Drupal::cache()->set($cache_key, $data, time() + 15);
      }
    }
    return $data;
  }

}
